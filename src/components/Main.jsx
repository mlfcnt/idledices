import React, { useState } from "react";
import Title from "./Title";
import Dice from "./Dice";

const Main = () => {
  function getResult(result) {
    return result;
  }
  return (
    <div>
      <Title />
      <Dice simple={getResult} />
      <Dice />
      {/* <div className="simple">
        <p>Total des dés : {simple(15)}</p>
      </div> */}
      {getResult()}
    </div>
  );
};

export default Main;
