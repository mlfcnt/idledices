import React from "react";

const Dice = props => {
  props.getResult(3);
  const generateRandomNumber = props => {
    return Math.floor(Math.random() * 6 + 1);
  };
  return <div className="dice">{generateRandomNumber()}</div>;
};

export default Dice;
